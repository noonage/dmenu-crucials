#!/usr/bin/env bash

# DMENU-CINEMA
# ------------

# Quickly play, clip, delete, download, or stream videos from dmenu

help() {
    read -d '' <<-EOF
		--dmenu="program"    - dmenu command
		                       (now: '${cmd_dmenu[@]}')
		--root="path"        - Folder of videos
		                       (now: '${root_dir[@]}')
		--mpv="program"      - Video player
		                       (now: '${cmd_mpv[@]}')
		--ytdl="program"     - Video downloader
		                       (now: '${cmd_ytdl[@]}')
		--term="program"     - Terminal for dmenu_cinema_ytdl & dmenu_cinema_mpv
		                       (now: '${cmd_term[@]}')
		--clip="program"     - Outputs system clipboard
		                       (now: '${cmd_clip[@]}')
		-h, --help           - Display this usage information
	EOF
    printf '%s' "$REPLY"
    exit 0
}

# CONFIG -----------------------------------------------------------------------

root_dir="$HOME/Videos"

# Default commands
cmd_dmenu=(dmenu -l 20 -p 'Vids' -i)
cmd_mpv=(dmenu_cinema_mpv)
cmd_ffmpeg=(ffmpeg)
cmd_ytdl=(dmenu_cinema_ytdl)
cmd_term=(alacritty --hold -e)
cmd_clip=(xclip -o)

ytdl_format="(bestvideo[height<=?1080]+bestaudio/best)" # limit to 1080p

# Custom yt-dlp command for dowloading videos, opens in an external terminal
# $1 = link
dmenu_cinema_ytdl() {
    "${cmd_term[@]}" yt-dlp \
                     -f "$ytdl_format" \
                     "$1" \
                     -o ""$cwd"/%(title)s.%(ext)s"
}

# Custom mpv command for playing videos, runs differently depending on file type
# Uses yt-dlp
# $1 = link
dmenu_cinema_mpv() {
    case "$1" in
        # Audio
        *mp3|*opus|*wav|*mpga|*aiff|*flac|*m4a|*ogg|*oga|*vox|*cda|*tta|*mpc|*act|*aa|*.unknown_video)
            setsid -f "${cmd_term[@]}" mpv \
                   --script-opts='ytdl_hook-ytdl_path=/usr/bin/yt-dlp' \
                   --audio-pitch-correction='no' \
                   --loop='inf' \
                   --no-audio-display "$1" ;;

        # Video
        *)
            setsid -f mpv \
                   --script-opts='ytdl_hook-ytdl_path=/usr/bin/yt-dlp' \
                   --no-terminal \
                   --ytdl-format="$ytdl_format" \
                   --loop='inf' \
                   --audio-pitch-correction='no' \
                   "$1" ;;
    esac
}

# Indicators
indc_dir='[D]'                                    # Directories
indc_parent='../'                                 # Parent directory
indc_del='-- Delete video --'                     # Delete mode
indc_clip='-- Make clip --'                       # Make clip mode
indc_down='-> Download'                           # Download from url
indc_stream='-> Stream'                           # Watch from url
indc_abort_del='[X] Abort deleting video in'      # Abort delete
indc_abort_clip='[X] Abort making clip in'        # Abort clip making
indc_abort_clip_time='Timestamp hh:mm:ss-hh:mm:ss, press to abort' # -----

# ------------------------------------------------------------------------------

# Parse command line options
while [[ -n $1 ]]; do
    case "$1" in
        --root=*)
            root_dir="${1#*=}"
            root_dir="${root_dir/\~/$HOME}" ;;
        --dmenu=*)
            cmd_dmenu=(${1#*=}) ;;
        --mpv=*)
            cmd_mpv=(${1#*=}) ;;
        --ytdl=*)
            cmd_ytdl=(${1#*=}) ;;
        --term=*)
            cmd_term=(${1#*=}) ;;
        --clip=*)
            cmd_clip=(${1#*=}) ;;
        --help|-h)
            help ;;
        *)
            printf '%s\n\n' "Error: unknown argument "\'$1\'""
            help
            exit 1 ;;
    esac
    shift
done

[[ -d $root_dir ]] || mkdir "$root_dir"

cwd="$root_dir"

while (( 1 )); do
    # Directory contents
    for vid in "$cwd"/*; do
        if [[ -d $vid ]]; then
            dirs+=("$indc_dir "${vid#$cwd/}"")
        else
            vids+=("${vid#$cwd/}")
        fi
    done

    # Parent directory
    [[ $cwd != $root_dir ]] && items+=("$indc_parent${cwd##*/}")

    # Adding options to view or download video if clipboard has a url
    link=$("${cmd_clip[@]}")
    case $link in
        http*|*www*|*.com*)
            items+=("$indc_down $link")
            items+=("$indc_stream $link") ;;
    esac

    items+=("$indc_del")
    items+=("$indc_clip")
    items+=("${dirs[@]}")
    items+=("${vids[@]}")

    # Main dmenu
    sel=$(printf '%s\n' "${items[@]}" | "${cmd_dmenu[@]}")
    [[ -z $sel ]] && exit 0

    case $sel in
        # Go to selected directory
        "$indc_dir"*)
            cwd="$cwd"/"${sel#"$indc_dir "}" ;;

        # Go to parent directory
        "$indc_parent"*)
            cwd="${cwd%/*}" ;;

        # View files and delete selected
        "$indc_del"*)
            items=("$indc_abort_del $cwd")
            items+=("${vids[@]}")
            sel="$(printf '%s\n' "${items[@]}" | "${cmd_dmenu[@]}")"
            [[ -z $sel ]] && exit 0

            if [[ $sel != "$indc_abort_del"* ]]; then
                if [[ -f $cwd/$sel ]]; then
                    rm "$cwd/$sel"
                    exit $?
                fi
            fi ;;

        # Download from url
        "$indc_down"*)
            "${cmd_ytdl[@]}" "$link"
            exit $? ;;

        # Stream from url
        "$indc_stream"*)
            "${cmd_mpv[@]}" "$link"
            exit $? ;;

        # Select a file, select timestamp, create clip with ffmpeg
        "$indc_clip")
            items=("$indc_abort_clip $cwd")
            items+=("${vids[@]}")
            sel="$(printf '%s\n' "${items[@]}" | "${cmd_dmenu[@]}")"
            [[ -z $sel ]] && exit 0

            if [[ $sel != "$indc_abort_clip"* ]]; then
                video="$cwd/$sel"
                if [[ -f $video ]]; then
                    timestamp="$(echo "$indc_abort_clip_time" | "${cmd_dmenu[@]}")"
                    [[ $timestamp == $indc_abort_clip_time ]] && exit 0
                    [[ -z $timestamp ]] && exit 0

                    "${cmd_term[@]}" "${cmd_ffmpeg[@]}" \
                        -i "$video" \
                        -ss "${timestamp%-*}" \
                        -to "${timestamp#*-}" -c copy "${video%.*}_clip.webm"
                    exit $?
                fi
            fi ;;

        # Play video file
        *)
            "${cmd_mpv[@]}" "$cwd/$sel"
            exit $? ;;
    esac

    unset dirs vids items
done
