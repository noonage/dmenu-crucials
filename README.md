# dmenu-crucials

Highly useful fully original scripts for dmenu written in pure bash.

## Installation

```
git clone git@gitlab.com:noonage/dmenu-crucials.git
cd dmenu-crucials
./install
```

### Uninstall

```
./install uninstall
```

## Included scripts

### Script: dmenu-editor-history

Provides a history list of opened files. With this you can keep track of previously opened files in order to quickly reopen them again. You can also search for new unopened files under the home directory (using find). If you are the sort of person that frequently edit the same files this is for you!

The default editor is sublime text, but the text editor, dmenu command, and find command can be changed with command line flags. See `--help` for details.

Shown below (with rofi):

![Screenshot of dmenu-editor history](https://gitlab.com/noonage/dmenu-crucials/-/raw/main/screenshots/editor-history.png)

#### Usage

There are two basic usages:

1) `dmenu-editor-history --sel` - Select a file or search for a new one via dmenu. This command is intended to be run via a custom hotkey.
2) `dmenu-editor-history --open example.txt` - Open a new file from the terminal. This command is intended to be run via a custom shell alias and replaces the normal command you use to open files like `subl`, `vim`, `code`.

#### dmenu tip

It takes a while for `find` to parse the home directory. Therefore it it advantageous to use a dmenu alternative (like `rofi` or `fzf`) that doesn't block standard in. This makes it so you don't have to wait on the `find` command to complete before you can interact with the list. There may also be patches for `dmenu` which can solve this "issue".

* Recommended `rofi` command: `dmenu-editor-history --dmenu="rofi -dmenu -i -p Hist" --sel`

### Script: dmenu-notes

A script for quickly opening and creating notes (text files). All your notes are stored in a root directory. Root defaults to `~/Notes`. The subdirectories inside root are called "categories". Inside dmenu the categories are listed first and then all your notes. Selecting a category will filter the notes to only show files residing in that directory. The default file extension is markdown. This script is compatible with [Obsidian](https://obsidian.md/) note directories.

The default editor is sublime text, but all important settings can be changed with command line flags. See `--help`.

Shown below (with rofi):

![Screenshot of dmenu-notes](https://gitlab.com/noonage/dmenu-crucials/-/raw/main/screenshots/notes.png)

### Script: dmenu-cinema

Provides a menu for quick access to play video files, deleting videos, make clips from videos, or download/stream a video from URL. Videos are stored in a root directory (default: `~/Videos`). If the clipboard contains a URL then extra options are added for dowloading and streaming from it using `mpv` and `yt-dlp` (defaults).

Shown below (with rofi):

![Screenshot of dmenu-cinema](https://gitlab.com/noonage/dmenu-crucials/-/raw/main/screenshots/cinema.png)

#### Some dependencies are required!

- `setsid` - used in custom included `mpv` function, required unless `mpv` command is changed
- All the default external programs can be listed with `--help`. You need a terminal emulator (not needed if `mpv` and `ytdl` are changed out), `dmenu` command, video player, video downloader, and a program which can fetch clipboard contents.

#### About mpv and yt-dlp

Using `mpv` and `yt-dlp` as is default is highly recommended. [mpv](https://mpv.io/) is a lightweight video player and [yt-dlp](https://github.com/yt-dlp/yt-dlp) is a command line video downloader. `dmenu-cinema` uses some custom wrapper functions for `mpv` and `yt-dlp`. The custom `mpv` function uses `setsid` to run mpv in a new external session. It also acts differently depending on file type (audio or video). The video resolution is also limited to 1080p.

### Script: dmenu-journal

A script for writing a daily journal. Much is similar to `dmenu-notes`. Journal entries are written in markdown (by default). Hitting `-- Today --` opens/creates the entry for the current date. `-- Read --` opens a program which let's you read the entire journal. `+ Earlier` is an option for creating an entry for an earlier date (only allows to go back max two weeks).

The default editor is sublime text. The pager used by `-- Read --` is by default the `alacritty` terminal emulator running `bat`. The "pager" needs to be a program which can open a space seperated list of filenames. See `--help` for settings that can be changed via command line flags.

#### Dependencies

- `dmenu-journal` uses the `date` command. It was written using the regular GNU coreutils version on Linux and no other versions are tested.

Shown below (with rofi):

![Screenshot of dmenu-journal](https://gitlab.com/noonage/dmenu-crucials/-/raw/main/screenshots/journal.png)

### Script: dmenu-pdfs

A script to quickly open PDFs stored in a directory with a PDF reader. The default directory is `~/Documents` and the default reader is `zathura`.
