#!/usr/bin/env bash

# DMENU-JOURNAL
# -------------

# Write a daily journal, administered from dmenu

help() {
    read -d '' <<-EOF
		--editor="program"   - Text editor    (now: '${cmd_editor[@]}')
		--dmenu="program"    - dmenu command  (now: '${cmd_dmenu[@]}')
		--pager="program"    - file reader    (now: '${cmd_pager[@]}')
		--root="path"        - Root directory (now: '$root_dir')
		--ext=".ext"         - File extension (now: '$file_ext')
		-h, --help           - Display this usage information
	EOF
    printf '%s' "$REPLY"
    exit 0
}

# CONFIG -----------------------------------------------------------------------

root_dir="$HOME/Journal"
file_ext='.md'
date_fmt='%Y-%m-%d-%A' # DON'T CHANGE, because sorting + other important things

# Title added to first line of journal files
title_str='# Journal entry for'
title_date_fmt='%A %d %B %Y'

# Default commands
cmd_dmenu=(dmenu -p 'Journ' -i -l 20)
cmd_editor=(subl --new-window)
cmd_pager=(alacritty -e bat --paging always --style='rule')

# Indicators
indc_today="-- Today --"
indc_yesterday="-- Yesterday --"
indc_read="-- Read --"
indc_earlier="+ Earlier"
indc_abort_earlier="[x] Abort creating an earlier entry"

# ------------------------------------------------------------------------------

while [[ -n $1 ]]; do
    case "$1" in
        --root=*)
            root_dir="${1#*=}"
            root_dir="${root_dir/\~/$HOME}" ;;
        --ext=*)
            file_ext="${1#*=}" ;;
        --dmenu=*)
            cmd_dmenu=(${1#*=}) ;;
        --pager=*)
            cmd_pager=(${1#*=}) ;;
        --editor=*)
            cmd_editor=(${1#*=}) ;;
        --help|-h)
            help ;;
        *) echo 'Error: unknown argument' && exit 1 ;;
    esac
    shift
done

[[ -d $root_dir ]] || mkdir "$root_dir"

deslug() {
    # Creates a nicer looking entry for dmenu from the filename
    # $1 = absolute slugged filepath to a file in $root_dir

    local file="${1#"$root_dir"/}"
    local file="${file%"$file_ext"}"
    local file="${file//-/' '}"

    printf '%s' "$file"
}

slug() {
    # Opposite of deslug() but with ability to slug multiple filenames at once
    # $@ = deslugged filenames

    for df in "${@}"; do
        local file="${df//' '/-}"
        local file="$file$file_ext"
        local file="$root_dir/$file"

        if [[ $count -eq 0 ]]; then
            printf '%s' "$file"
        else
            printf ' %s' "$file"
        fi

        (( count++ ))
    done
}

# Reading directory
for f in "$root_dir"/*; do
    [[ -f $f ]] && entries=("$(deslug $f)" "${entries[@]}")
done

all_entries+=(
    "$indc_today"
    "$indc_yesterday"
    "$indc_read"
    "$indc_earlier"
    "${entries[@]}"
)

create_file() {
    # Creates file with a title if necessary, and opens it in $cmd_editor
    # $1 = a date in $date_fmt

    local file="$root_dir/$1$file_ext"
    if ! [[ -f "$file" ]]; then
        date -d "$1" "+$title_str $title_date_fmt%n%n" > "$file"
    fi

    "${cmd_editor[@]}" "$file"
}

while (( 1 )); do
    sel="$(printf "%s\n" "${all_entries[@]}" | "${cmd_dmenu[@]}")"

    case "$sel" in
        '') exit ;;

        "$indc_today")
            date=$(date +"$date_fmt")
            create_file "$date" ;;

        "$indc_yesterday")
            date=$(date +"$date_fmt" -d "-1 day")
            create_file "$date";;

        "$indc_read")
            "${cmd_pager[@]}" $(slug "${entries[@]}") ;;

        "$indc_earlier")
            date_seq+=("$indc_abort_earlier")

            for (( i = 1; i < 15; i++ )); do
                date_seq+=("$(date +"$(deslug "$date_fmt")" -d "-$i day")")
            done

            sel_date="$(printf "%s\n" "${date_seq[@]}" | "${cmd_dmenu[@]}")"

            case "$sel_date" in
                '') exit ;;

                "$indc_abort_earlier")
                    # Go back to home screen
                    unset date_seq
                    continue ;;

                *)
                    create_file "${sel_date//' '/-}" ;;
            esac ;;

        *)
            file="$(slug "$sel")"
            [[ -f $file ]] && "${cmd_editor[@]}" "$file" ;;
    esac

    # Exit program
    break
done
